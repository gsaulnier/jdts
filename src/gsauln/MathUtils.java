/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Provide useful functions not found in the Apache package that are math related.
 */
public final class MathUtils
{

	private MathUtils() {}

	/**
	 * Return the object that has majority (# of occurrences) in the collection. 
	 * @param collection collection to extract the object that is in majority from.
	 * @param <T> Type of element to get the majority for. Must implement equals and hashcode.  
	 * @return the object that has majority (# of occurrences) in the collection. 
	 */
	public static <T> T majority(Collection<T> collection)
	{
		HashMap<T, Integer> counts = new HashMap<T, Integer>();
		for (T aT : collection)
		{
			Integer count = counts.get(aT);
			if (count == null)
			{
				counts.put(aT, 0);
				count = counts.get(aT);
			}
			counts.put(aT, count+1);
		}
		T majority = null;
		int maxCount = -1;
		for (T aT : counts.keySet())
		{
			if (majority == null)
			{
				majority = aT;
				maxCount = counts.get(aT);
			}

			int count = counts.get(aT);
			if (maxCount < count)
			{
				majority = aT;
				maxCount = count;
			}
		}
		return majority;
	}

	/**
	 * Return the counts of each elements present in the collection.
	 * @param collection collection to extract the counts of each elements from.
	 * @return the counts of each elements present in the collection.
	 * @param <T> Type of element to get the counts for. Must implement equals and hashcode.  
	 */
	public static <T> HashMap<T, Integer> getCounts(ArrayList<T> collection)
	{
		HashMap<T, Integer> counts = new HashMap<T, Integer>();
		
		// Compute the count
		for (T content : collection)
		{
			Integer value = counts.get(content);
			if (value == null)
			{
				value = 0; 
			}
			value = value + 1;
			counts.put(content, value);
		}
		
		return counts;
	}


}
