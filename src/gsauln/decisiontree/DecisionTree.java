/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.logic.ANode;

import java.io.Serializable;

/**
 * Implements a decision tree.
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 */
public class DecisionTree<P extends IPoint<?, ?>, LC extends Serializable> implements Serializable
{

	private static final long serialVersionUID = 1L;

	private ANode<P, LC> fRoot;
	
	/**
	 * Set the root of the decision tree to the given root.
	 * @param root root of the decision tree.
	 */
	public DecisionTree(ANode<P, LC> root)
	{
		fRoot = root;
	}
	
	/**
	 * Return the content of the leaf reached when evaluating the decision tree.
	 * @param point point for which to evaluate the decision tree on.
	 * @return the content of the leaf reached when evaluating the decision tree.
	 */
	public LC classify(P point)
	{
		return fRoot.evaluate(point);
	}
	
	/**
	 * Return the root of the decision tree.
	 * @return the root of the decision tree.
	 */
	public ANode<P, LC> getRoot()
	{
		return fRoot;
	}
	
	/**
	 * Print the full tree structure.
	 * @return the full tree structure.
	 */
	public String printTree()
	{
		return ANode.printSubTree(fRoot);
	}
}
