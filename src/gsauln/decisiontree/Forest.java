/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree;

import gsauln.decisiontree.interfaces.IPoint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Implements a forest of decision trees. 
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 */
public class Forest<P extends IPoint<?, ?>, LC extends Serializable> implements Serializable
{

	private static final long serialVersionUID = 1L;

	private ArrayList<DecisionTree<P, LC>> fTrees;
	
	/**
	 * Creates a new empty forest.
	 */
	public Forest()
	{
		fTrees = new ArrayList<DecisionTree<P, LC>>();
	}
	
	/**
	 * Add the tree to the forest.
	 * @param tree tree to add to the forest.
	 */
	public void add(DecisionTree<P, LC> tree)
	{
		fTrees.add(tree);
	}
	
	/**
	 * Add all the trees to the forest.
	 * @param trees trees to add to the forest.
	 */
	public void addAll(Collection<DecisionTree<P, LC>> trees)
	{
		fTrees.addAll(trees);
	}
	
	/**
	 * Return the size of the forest (number of trees contained in it).
	 * @return the size of the forest (number of trees contained in it).
	 */
	public int size()
	{
		return fTrees.size();
	}
	
	/**
	 * Return the trees contained in the forest.
	 * @return the trees contained in the forest.
	 */
	public ArrayList<DecisionTree<P, LC>> getTrees()
	{
		return new ArrayList<DecisionTree<P, LC>>(fTrees);
	}
	
	/**
	 * Return the contents of the reached leaf in each decision trees.
 	 * @param point point for which to evaluate each decision tree on.
	 * @return the contents of the reached leaf in each decision trees.
	 */
	public ArrayList<LC> classify(P point)
	{
		ArrayList<LC> contents = new ArrayList<LC>(fTrees.size());
		for (DecisionTree<P, LC> tree : fTrees)
		{
			LC content = tree.classify(point);
			contents.add(content);
		}
		return contents;
	}
	
	/**
	 * Print the full content of the forest.  The structure of each tree will be printed.
	 * @return the full content of the forest.  The structure of each tree will be printed.
	 */
	public String printForest()
	{
		StringBuilder str = new StringBuilder();
		str.append("Forest(");
		for (int i = 0; i < fTrees.size()-1; i++)
		{
			str.append(fTrees.get(i).printTree());
			str.append(",");
		}
		str.append(fTrees.get(fTrees.size()-1).printTree());
		str.append(")");
		return str.toString();
	}
}
