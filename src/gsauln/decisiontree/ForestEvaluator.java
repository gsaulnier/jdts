/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree;


import gsauln.MathUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.math.stat.StatUtils;

/**
 * Useful function to analyse the output of a forest.
 */
public final class ForestEvaluator
{

	private ForestEvaluator() {}
	
	/**
	 * Return the element that is in majority in the classifications.
	 * @param classifications the classifications to compute the class in majority from.
	 * @return the element that is in majority in the classifications.
	 * @param <LC> Type of element present in the classifications. Must implement equals and hashcode.
	 */
	public static <LC extends Serializable> LC getMajority(ArrayList<LC> classifications)
	{
		LC majorityElement = MathUtils.majority(classifications);
		return majorityElement;
	}
	
	/**
	 * Return the fraction of trees that classified the point as any classes present in classes.
	 * @param classifications classifications to compute the fraction from.
	 * @param classes classes to compute the fraction for.
	 * @return the fraction of trees that classified the point as any classes present in classes.
	 * @param <LC> Type of element present in the classifications. Must implement equals and hashcode.
	 */
	public static <LC extends Serializable> double getFraction(ArrayList<LC> classifications, Collection<LC> classes)
	{
		HashMap<LC, Integer> counts = MathUtils.getCounts(classifications);
		Double value = 0.0;
		for (LC clazz : classes)
		{
			Integer count = counts.get(clazz);
			if (count != null)
			{
				value += count;
			}
		}

		return value/classifications.size();
	}
	
	/**
	 * Return the mean of the numbers in the classifications.
	 * @param classifications the classifications to compute the mean from.
	 * @return the mean of the numbers in the classifications.
	 * @param <LC> Type of numbers present in the classifications.
	 */
	public static <LC extends Number> double getMean(ArrayList<LC> classifications)
	{
		double[] evaluations = new double[classifications.size()];
		for (int i = 0; i < evaluations.length; i++)
		{
			double value = classifications.get(i).doubleValue();
			evaluations[i] = value;
		}
		double mean = StatUtils.mean(evaluations);
		return mean;
	}
}
