/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import gsauln.decisiontree.exceptions.DecisionTreeException;
import gsauln.decisiontree.interfaces.IPoint;

/**
 * Implementation of a point using strings as keys for attributes and doubles as values. 
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 * @param <V> Type of attribute values.
 */
public class Point<K extends Serializable, V extends Serializable> implements IPoint<K, V>
{

	private static final long serialVersionUID = 1L;

	private HashMap<K, V> fValues;
	
	/**
	 * Create a new point with the given feature values.
	 * @param values pair of attribute name and corresponding value.
	 */
	public Point(Map<K, V> values)
	{	
		fValues = new HashMap<K, V>(values);
	}
	
	@Override
	public V getValue(K attribute)
	{
		V value = fValues.get(attribute);
		if (value == null)
		{
			throw new DecisionTreeException("The attribute name is not present in the point! name: "+attribute);
		}
		return value;
	}

	@Override
	public HashSet<K> getAttributes()
	{
		HashSet<K> names = new HashSet<K>(fValues.keySet());
		return names;
	}

	@Override
	public int size()
	{
		return fValues.size();
	}

}
