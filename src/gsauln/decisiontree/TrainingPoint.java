/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree;

import java.io.Serializable;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;

/**
 * Implementation of a training point using a Point as a base and a boolean as content (i.e. label).
 * @param <P> Type of point used with the decision tree during query.
 * @param <TC> Type of training content attached to the point for training purposes (enums are useful to define classes). 
 * Must implement equals and hashcode. 
 */
public class TrainingPoint<P extends IPoint<?, ?>, TC extends Serializable> implements ITrainingPoint<P, TC>
{

	private static final long serialVersionUID = 1L;
	
	private P fPoint;
	private TC fLabel;
	
	/**
	 * Create a new point with the given feature values and the label as content for training.
	 * @param point point for which to add training content.
	 * @param label boolean label corresponding to the training content of the point.
	 */
	public TrainingPoint(P point, TC label)
	{
		fPoint = point;
		fLabel = label;
	}

	@Override
	public P getPoint()
	{
		return fPoint;
	}
	
	@Override
	public TC getTrainingContent()
	{
		return fLabel;
	}

}
