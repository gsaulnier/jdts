/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.exceptions;

/**
 * Exception class to handle illegal operations in the tree package. 
 */
public class DecisionTreeException extends RuntimeException
{

	private static final long serialVersionUID = 1L;

	/**
	 * Generates a standard error message.
	 * @param pMessage The message to store.
	 */
	public DecisionTreeException(String pMessage) 
	{
		super(pMessage);
	}
}
