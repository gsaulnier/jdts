/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.factory;

import gsauln.decisiontree.DecisionTree;
import gsauln.decisiontree.Forest;
import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.logic.Gardener;
import gsauln.decisiontree.state.interfaces.ITrainingState;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

import jparfor.Functor;
import jparfor.MultiThreader;
import jparfor.Range;

/**
 * Basic decision tree factory that is able to handle the creation of a reproducible forest 
 * with respect to random number generation. Any call to growTree or growForest changes the state
 * of the random number generator.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public abstract class ADecisionTreeFactory<TP extends ITrainingPoint<P, ?>, 
P extends IPoint<?, ?>, 
LC extends Serializable, 
TS extends ITrainingState<TS, TP>>
implements IDecisionTreeFactory<TP, P, LC, TS>
{

	private static final long serialVersionUID = 1L;

	private Random fRandom;
	
	/**
	 * Creates a new decision tree factory.
	 */
	public ADecisionTreeFactory()
	{
		fRandom = new Random();
	}
	
	/**
	 * Creates a factory that will use the given random number generator. Does not create a copy.
	 * @param random random the random number generator to be used with this factory.
	 */
	public ADecisionTreeFactory(Random random)
	{
		fRandom = random;
	}
	
	@Override
	public DecisionTree<P, LC> growTree(Collection<TP> set)
	{
		Gardener<TP, P, LC, TS> gardener = getGardener(fRandom);
		return gardener.growTree(set, getInitialTrainingState(fRandom));
	}

	@Override
	public Forest<P, LC> growForest(final Collection<TP> set, int numberOfTrees)
	{
		Forest<P, LC> forest = new Forest<P, LC>();
		
		final ArrayList<Gardener<TP, P, LC, TS>> crew = new ArrayList<Gardener<TP, P, LC, TS>>(numberOfTrees);
		final ArrayList<TS> trainingStates = new ArrayList<TS>(numberOfTrees);
		for(int i = 0; i < numberOfTrees; i++)
		{
			crew.add(getGardener(new Random(fRandom.nextLong())));
			trainingStates.add(getInitialTrainingState(fRandom));
		}
		
		ArrayList<DecisionTree<P, LC>> trees = MultiThreader.foreach(new Range(numberOfTrees), new Functor<Integer, DecisionTree<P, LC>>()
				{
					@Override
					public DecisionTree<P, LC> function(Integer input)
					{
						DecisionTree<P, LC> tree = crew.get(input).growTree(set, trainingStates.get(input));
						return tree;
					}
				});
		
		forest.addAll(trees);
		
		return forest;
	}

	/**
	 * Set the random number generator used by the factory to the given one.  Does not create a copy.
	 * @param random the random number generator to be used with this factory.
	 */
	public void setRandom(Random random)
	{
		fRandom = random;
	}
	
	/**
	 * Returns the random number generator used by this factory.  Does not return a copy.
	 * @return the random number generator used by this factory.
	 */
	public Random getRandom()
	{
		return fRandom;
	}
}
