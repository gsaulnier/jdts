/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.factory;

import java.io.Serializable;
import java.util.Random;

import gsauln.decisiontree.Point;
import gsauln.decisiontree.TrainingPoint;
import gsauln.decisiontree.split.strategy.scoring.GeneralizedNormalizedShannonEntropy;

/**
 * Create an Extra-Tree factory.
 * 
 * A leaf is created when:
 * 	- less than nMin points are present in the training set
 *  - all attributes of the points are constant
 *  - all the training content of the points is the same
 * 
 * A leaf is created by picking the majority of the training content of the points.
 * 
 * An inner node is created by selecting k non-constant attributes at random with replacement).  Then a threshold
 * is picked uniformly at random across the attribute range.  The split maximizing the generalized  normalized Shannon
 * entropy.
 *
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 * @param <TC> Type of training content attached to the point for training purposes (enums are useful to define classes). 
 * Must implement equals and hashcode. 
 */
public class DefaultExtraTreeClassificationFactory<	K extends Serializable, 
														TC extends Serializable> 
		extends ExtraTreeClassificationFactory<TrainingPoint<Point<K, Double>, TC>, Point<K, Double>, TC, K>
{

	private static final long serialVersionUID = 1L;

	/**
	 * Create an Extra-Tree factory.
	 * 
	 * 	A leaf is created when:
	 * 	- less than nMin points are present in the training set
	 *  - all attributes of the points are constant
	 *  - all the training content of the points is the same
	 * 
	 * A leaf is created by picking the majority of the training content of the points.
	 * 
	 * An inner node is created by selecting k non-constant attributes at random with replacement).  Then a threshold
	 * is picked uniformly at random across the attribute range.  The split maximizing the generalized  normalized Shannon
	 * entropy.
	 * 
	 * @param k number of attributes to select at random when creating splits.
	 * @param nMin minimum number of points to be present in the set in order to create an inner node.
	 */
	public DefaultExtraTreeClassificationFactory(int k, int nMin)
	{
		super(k, true, nMin, new GeneralizedNormalizedShannonEntropy<TrainingPoint<Point<K, Double>, TC>, Point<K, Double>, TC>());
	}
	
	/**
	 * Create an Extra-Tree factory.
	 * 
	 * 	A leaf is created when:
	 * 	- less than nMin points are present in the training set
	 *  - all attributes of the points are constant
	 *  - all the training content of the points is the same
	 * 
	 * A leaf is created by picking the majority of the training content of the points.
	 * 
	 * An inner node is created by selecting k non-constant attributes at random with replacement).  Then a threshold
	 * is picked uniformly at random across the attribute range.  The split maximizing the generalized  normalized Shannon
	 * entropy.
	 * 
	 * @param k number of attributes to select at random when creating splits.
	 * @param nMin minimum number of points to be present in the set in order to create an inner node.
	 * @param random the random number generator to be used with this factory.
	 */
	public DefaultExtraTreeClassificationFactory(int k, int nMin, Random random)
	{
		super(k, true, nMin, new GeneralizedNormalizedShannonEntropy<TrainingPoint<Point<K, Double>, TC>, Point<K, Double>, TC>(), random);
	}
}
