/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.factory;

import java.io.Serializable;
import java.util.Random;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.leaf.certificationstrategy.CertifierAggregator;
import gsauln.decisiontree.leaf.certificationstrategy.ConstantTrainingContent;
import gsauln.decisiontree.leaf.certificationstrategy.TrainingSetSize;
import gsauln.decisiontree.leaf.creationstrategy.Majority;
import gsauln.decisiontree.logic.Gardener;
import gsauln.decisiontree.split.creation.UniformDecisionStumpCreator;
import gsauln.decisiontree.split.strategy.RandomizedNonConstantSplitCreator;
import gsauln.decisiontree.split.strategy.scoring.interfaces.IScoringFunction;
import gsauln.decisiontree.state.NonConstantAttributesState;

/**
 * Create an Extra-Tree factory.
 * 
 * A leaf is created when:
 * 	- less than nMin points are present in the training set
 *  - all attributes of the points are constant
 *  - all the training content of the points is the same
 * 
 * A leaf is created by picking the majority of the training content of the points.
 * 
 * An inner node is created by selecting k non-constant attributes at random (with or without replacement).  Then a threshold
 * is picked uniformly at random across the attribute range.  The split maximizing the scoring function is then used in the
 * node.
 * 
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <TC> Type of training content attached to the point for training purposes (enums are useful to define classes). 
 * Must implement equals and hashcode. 
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 */
public class ExtraTreeClassificationFactory<	TP extends ITrainingPoint<P, TC>, 
												P extends IPoint<K, Double>, 
												TC extends Serializable, 
												K extends Serializable> 
												extends ADecisionTreeFactory<TP, P, TC, NonConstantAttributesState<TP, P, K>>
{

	private static final long serialVersionUID = 1L;
	
	private int fK;
	private boolean fReplacement;
	private int fNmin;
	private IScoringFunction<TP, P> fScoringFunction;
	
	/**
	 * Create an Extra-Tree factory.
	 * 
	 * A leaf is created when:
	 * 	- less than nMin points are present in the training set
	 *  - all attributes of the points are constant
	 *  - all the training content of the points is the same
	 * 
	 * A leaf is created by picking the majority of the training content of the points.
	 * 
	 * An inner node is created by selecting k non-constant attributes at random (with or without replacement).  Then a threshold
	 * is picked uniformly at random across the attribute range.  The split maximizing the scoring function is then used in the
	 * node.
	 * 
	 * 
	 * @param k number of attributes to select at random when creating splits.
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param nMin minimum number of points to be present in the set in order to create an inner node.
	 * @param scoringFunction scoring function used to select the optimal split (maximized).
	 */
	public ExtraTreeClassificationFactory(int k, boolean replacement, int nMin, IScoringFunction<TP, P> scoringFunction)
	{
		super();
		init(k, replacement, nMin, scoringFunction);
	}
	
	/**
	 * Create an Extra-Tree factory.
	 * 
	 * A leaf is created when:
	 * 	- less than nMin points are present in the training set
	 *  - all attributes of the points are constant
	 *  - all the training content of the points is the same
	 * 
	 * A leaf is created by picking the majority of the training content of the points.
	 * 
	 * An inner node is created by selecting k non-constant attributes at random (with or without replacement).  Then a threshold
	 * is picked uniformly at random across the attribute range.  The split maximizing the scoring function is then used in the
	 * node.
	 * 
	 * 
	 * @param k number of attributes to select at random when creating splits.
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param nMin minimum number of points to be present in the set in order to create an inner node.
	 * @param scoringFunction scoring function used to select the optimal split (maximized).
	 * @param random the random number generator to be used with this factory.
	 */
	public ExtraTreeClassificationFactory(int k, boolean replacement, int nMin, IScoringFunction<TP, P> scoringFunction, Random random)
	{
		super(random);
		init(k, replacement, nMin, scoringFunction);
	}
	
	/**
	 * Initialize the factory with the given parameters.
	 * @param k number of attributes to select at random when creating splits.
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param nMin minimum number of points to be present in the set in order to create an inner node.
	 * @param scoringFunction scoring function used to select the optimal split (maximized).
	 */
	private void init(int k, boolean replacement, int nMin, IScoringFunction<TP, P> scoringFunction)
	{
		fK = k;
		fReplacement = replacement;
		fNmin = nMin;
		fScoringFunction = scoringFunction;
	}
	
	@Override
	public Gardener<TP, P, TC, NonConstantAttributesState<TP, P, K>> getGardener(Random random)
	{
		// create the leaf certifier
		CertifierAggregator<TP, NonConstantAttributesState<TP, P, K>> leafCertifier = 
				new CertifierAggregator<TP, NonConstantAttributesState<TP, P, K>>();
		leafCertifier.add(new ConstantTrainingContent<TP, TC, NonConstantAttributesState<TP, P, K>>());
		leafCertifier.add(new TrainingSetSize<TP, NonConstantAttributesState<TP, P, K>>(fNmin));
		
		// create the leaf creator
		Majority<TP, P, TC, NonConstantAttributesState<TP, P, K>> leafCreator = new Majority<TP, P, TC, NonConstantAttributesState<TP, P, K>>();
		
		// create the split creator
		UniformDecisionStumpCreator<TP, P, K, NonConstantAttributesState<TP, P, K>> uDSCreator = 
				new UniformDecisionStumpCreator<TP, P, K, NonConstantAttributesState<TP, P, K>>(random);
		RandomizedNonConstantSplitCreator<TP, P, K, NonConstantAttributesState<TP, P, K>> splitCreator = 
				new RandomizedNonConstantSplitCreator<TP, P, K, NonConstantAttributesState<TP, P, K>>(fK, 
																										fReplacement, 
																										uDSCreator, 
																										fScoringFunction, 
																										random);
		
		// create the gardener
		Gardener<TP, P, TC, NonConstantAttributesState<TP, P, K>> gardener = 
				new Gardener<TP, P, TC, NonConstantAttributesState<TP, P, K>>(splitCreator, leafCertifier, leafCreator);
		return gardener;
	}

	@Override
	public NonConstantAttributesState<TP, P, K> getInitialTrainingState(Random random)
	{
		return new NonConstantAttributesState<TP, P, K>();
	}

}
