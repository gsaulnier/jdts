/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.interfaces;

import gsauln.decisiontree.state.interfaces.ITrainingState;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface specifying the function required by the gardener to verify if a leaf should be created.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public interface ILeafCertificationStrategy<TP extends ITrainingPoint<?, ?>, TS extends ITrainingState<TS, TP>> extends Serializable
{

	/**
	 * Returns true if a leaf should be created, false otherwise (an inner node will be created).
	 * @param set set of points to from which to verify if a leaf should be created.
	 * @param trainingState training state of the tree at the node for which the function is called.
	 * @return true if a leaf should be created, false otherwise (an inner node will be created).
	 */
	boolean isLeaf(Collection<TP> set, TS trainingState);
	
}
