/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.interfaces;

import gsauln.decisiontree.logic.Leaf;
import gsauln.decisiontree.state.interfaces.ITrainingState;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface specifying the function required by the gardener to create a leaf in the decision tree.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public interface ILeafCreationStrategy<	TP extends ITrainingPoint<P, ?>, 
											P extends IPoint<?, ?>, 
											LC extends Serializable, 
											TS extends ITrainingState<TS, TP>> extends Serializable
{

	/**
	 * Return a leaf created from the set of points and the training state of the decision tree.
	 * @param set set of points to create the leaf from.
	 * @param trainingState training state of the tree at the node for which the function is called.
	 * @return a leaf created from the set of points and the training state of the decision tree.
	 */
	Leaf<P, LC> createLeaf(Collection<TP> set, TS trainingState);
	
}
