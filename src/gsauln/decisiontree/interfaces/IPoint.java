/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.interfaces;

import java.io.Serializable;
import java.util.HashSet;


/**
 * Interface for points used in the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 * @param <V> Type of attribute values.
 */
public interface IPoint<K extends Serializable, V extends Serializable> extends Serializable
{
	
	/**
	 * Return the value of the given attribute.
	 * @param attribute the attribute for which to return the value for.
	 * @return the values of the given feature.
	 */
	V getValue(K attribute);
	
	/**
	 * Return the attributes contained in the Point.
	 * @return the attributes contained in the Point.
	 */
	HashSet<K> getAttributes();
	
	/**
	 * Return the number of attributes contained in the Point.
	 * @return the number of attributes contained in the Point.
	 */
	int size();
}
