/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.interfaces;

import java.io.Serializable;

/**
 * Interface for a decision tree inner node split.
 * @param <P> Type of point used with the decision tree during query.
 */
public interface ISplit<P extends IPoint<?, ?>> extends Serializable
{

	/**
	 * Return true, if the split is passed (going right) and false otherwise (going left).
	 * @param point point to evaluate the test on.
	 * @return true, if the split is passed (going right) and false otherwise (going left).
	 */
	boolean evaluate(P point);
	
}
