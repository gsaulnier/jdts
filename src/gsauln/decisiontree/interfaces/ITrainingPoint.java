/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.interfaces;

import java.io.Serializable;


/**
 * Interface to be implemented by points used in the training of decision trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <TC> Type of training content attached to the point for training purposes (enums are useful to define classes). 
 * Must implement equals and hashcode. 
 */
public interface ITrainingPoint<P extends IPoint<?, ?>, TC extends Serializable> extends Serializable
{
	/**
	 * Return the point associated with this training point.
	 * @return the point associated with this training point.
	 */
	P getPoint();
	
	/**
	 * Return the label of the Point.
	 * @return the label of the Point.
	 */
	TC getTrainingContent();
	
}
