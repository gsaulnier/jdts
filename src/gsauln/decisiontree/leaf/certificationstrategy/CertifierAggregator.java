/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.leaf.certificationstrategy;

import java.util.ArrayList;
import java.util.Collection;

import gsauln.decisiontree.interfaces.ILeafCertificationStrategy;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Certifier that aggregates other certifiers.  Will return true for isLeaf if any certifier returns true, returns false otherwise.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <TS> Type of training state implementing this interface. 
 */
public class CertifierAggregator<TP extends ITrainingPoint<?, ?>, TS extends ITrainingState<TS, TP>> implements ILeafCertificationStrategy<TP, TS>
{

	private static final long serialVersionUID = 1L;
	
	private ArrayList<ILeafCertificationStrategy<TP, TS>> fStrategies;
	
	/**
	 * Create a new aggregator return true for isLeaf if any certifier returns true, returns false otherwise.
	 */
	public CertifierAggregator()
	{
		fStrategies = new ArrayList<ILeafCertificationStrategy<TP, TS>>();
	}
	
	/**
	 * Add a certifying strategy to this aggregator.
	 * @param strategy certifying strategy to add to this aggregator.
	 */
	public void add(ILeafCertificationStrategy<TP, TS> strategy)
	{
		fStrategies.add(strategy);
	}
	
	@Override
	public boolean isLeaf(Collection<TP> set, TS trainingState)
	{
		for (ILeafCertificationStrategy<TP, TS> strategy : fStrategies)
		{
			if (strategy.isLeaf(set, trainingState))
			{
				return true;
			}
		}
		return false;
	}

}
