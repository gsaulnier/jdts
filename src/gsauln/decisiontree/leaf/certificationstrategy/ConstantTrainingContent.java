/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.leaf.certificationstrategy;

import java.io.Serializable;
import java.util.Collection;

import gsauln.decisiontree.interfaces.ILeafCertificationStrategy;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Certifier that return true to isLeaf if all the training content of the points in the set are the same. 
 *
 * @param <TP> Type of training points used during the creation of the trees. 
 * @param <TC> Type of training content attached to the point for training purposes (enums are useful to define classes). 
 * Must implement equals and hashcode. 
 * @param <TS> Type of training state implementing this interface.
 */
public class ConstantTrainingContent<	TP extends ITrainingPoint<?, TC>, 
									  	TC extends Serializable, 
									  	TS extends ITrainingState<TS, TP>> implements ILeafCertificationStrategy<TP, TS>
{

	private static final long serialVersionUID = 1L;

	@Override
	public boolean isLeaf(Collection<TP> set, TS trainingState)
	{
		TC content = null;
		for (TP tp : set)
		{
			if (content == null)
			{
				content = tp.getTrainingContent();
			}
			if (!content.equals(tp.getTrainingContent()))
			{
				return false;
			}
		}
		return true;
	}

}
