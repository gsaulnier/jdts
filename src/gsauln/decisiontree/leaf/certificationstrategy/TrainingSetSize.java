/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.leaf.certificationstrategy;

import java.util.Collection;

import gsauln.decisiontree.interfaces.ILeafCertificationStrategy;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Certifier that returns true to isLeaf if less than nMin points are present in the training set. 
 *
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <TS> Type of training state implementing this interface. 
 */
public class TrainingSetSize<TP extends ITrainingPoint<?, ?>, TS extends ITrainingState<TS, TP>> implements ILeafCertificationStrategy<TP, TS> 
{

	private static final long serialVersionUID = 1L;
	
	private int fNmin;
	
	/**
	 * Creates a certifier that returns true to isLeaf if less than nMin points are present in the training set. 
	 * @param nMin minimum number of points to be present in the set in order to create an inner node.
	 */
	public TrainingSetSize(int nMin)
	{
		fNmin = nMin;
	}
	
	@Override
	public boolean isLeaf(Collection<TP> set, TS trainingState)
	{
		if (set.size() < fNmin) 
		{
			return true;
		}
		return false;
	}

}
