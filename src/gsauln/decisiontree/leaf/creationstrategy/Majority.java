/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.leaf.creationstrategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;


import gsauln.MathUtils;
import gsauln.decisiontree.interfaces.ILeafCreationStrategy;
import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.logic.Leaf;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Create a leaf creator that will create a leaf with the element in majority across the training content of the points. 
 *
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <TC> Type of training content attached to the point for training purposes (enums are useful to define classes). 
 * Must implement equals and hashcode. 
 * @param <TS> Type of training state used during the creation of the trees.
 */
public class Majority<	TP extends ITrainingPoint<P, TC>, 
						P extends IPoint<?, ?>, 
						TC extends Serializable, 
						TS extends ITrainingState<TS, TP>> implements ILeafCreationStrategy<TP, P, TC, TS>
{

	private static final long serialVersionUID = 1L;

	@Override
	public Leaf<P, TC> createLeaf(Collection<TP> set, TS trainingState)
	{
		ArrayList<TC> trainingContent = new ArrayList<TC>();
		for (TP tp : set)
		{
			trainingContent.add(tp.getTrainingContent());
		}
		TC majority = MathUtils.majority(trainingContent);
		return new Leaf<P, TC>(majority);
	}

}
