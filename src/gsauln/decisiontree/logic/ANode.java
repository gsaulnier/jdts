/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.logic;

import gsauln.decisiontree.interfaces.IPoint;

import java.io.Serializable;


/**
 * Node object for binary trees. 
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 */
public abstract class ANode<P extends IPoint<?, ?>, LC extends Serializable> implements Serializable
{

	private static final long serialVersionUID = 1L;
	
	private ANode<P, LC> fLeftChild;
	private ANode<P, LC> fParent;
	private ANode<P, LC> fRightChild;
	
	/**
	 * Creates a binary node (max 2 children).
	 */
	public ANode()
	{
		fParent = null;
		fLeftChild = null;
		fRightChild = null;
	}
	
	/**
	 * Return the left child of the node.
	 * @return the left child of the node.
	 */
	public ANode<P, LC> getLeftChild()
	{
		return fLeftChild;
	}
	
	/**
	 * Return the parent of the node.
	 * @return the parent of the node.
	 */
	public ANode<P, LC> getParent()
	{
		return fParent;
	}
	
	/**
	 * Return the right child of the node.
	 * @return the right child of the node.
	 */
	public ANode<P, LC> getRightChild()
	{
		return fRightChild;
	}
	
	/**
	 * Return true if the node is a leaf (i.e. both children are null), false otherwise.
	 * @return true if the node is a leaf (i.e. both children are null), false otherwise.
	 */
	public boolean isLeaf()
	{
		return fLeftChild == null && fRightChild == null;
	}
	
	/**
	 * Set the left child of the node. (Only a reference is stored)
	 * @param leftChild left child of the node.
	 */
	public void setLeftChild(ANode<P, LC> leftChild)
	{
		fLeftChild = leftChild;
	}
	
	/**
	 * Set the parent of the node. (Only a reference is stored)
	 * @param parent parent of the node.
	 */
	public void setParent(ANode<P, LC> parent)
	{
		fParent = parent;
	}
	
	/**
	 * Set the right child of the node. (Only a reference is stored)
	 * @param rightChild right child of the node.
	 */
	public void setRightChild(ANode<P, LC> rightChild)
	{
		fRightChild = rightChild;
	}
	
	/**
	 * Return the content of the leaf reached when evaluating the decision tree rooted at this node.
	 * When a test is passed (true) the right child is visited, otherwise the left child is visited.
	 * When a leaf is reached, the content of the leaf is returned.
	 * @param point point for which to evaluate the decision tree rooted at this node on.
	 * @return the content of the leaf reached when evaluating the decision tree rooted at this node.
	 */
	public abstract LC evaluate(P point);
	
	@Override
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append("[");
		str.append(nodeToString(fParent));
		str.append(",");
		str.append(nodeToString(fLeftChild));
		str.append(",");
		str.append(nodeToString(fRightChild));
		str.append("]");
		return str.toString();
	}
	
	/**
	 * Return the hashcode of the node or if the node is null, "null" is returned.
	 * @param node node for which to return the string representation.
	 * @return the hashcode of the node or if the node is null, "null" is returned.
	 */
	public String nodeToString(ANode<P, LC> node)
	{
		if (node == null)
		{
			return "null";
		}
		else
		{
			return String.valueOf(node.hashCode());
		}
	}
	
	/**
	 * Return the description of the subtree rooted at the given node.
	 * @param root root for which to return the subtree description.
	 * @param <P> Type of point used with the node.
	 * @param <LC> Type of content present in the leafs..
	 * @return the description of the subtree rooted at the given node.
	 */
	public static <P extends IPoint<?, ?>, LC extends Serializable> String printSubTree(ANode<P, LC> root)
	{
		StringBuilder str = new StringBuilder();
		str.append("[");
		str.append(root.getInfo());
		str.append(",");
		if (root.getLeftChild() != null)
		{
			str.append(printSubTree(root.getLeftChild()));
		}
		else
		{
			str.append("null");
		}
		str.append(",");
		if (root.getRightChild() != null)
		{
			str.append(printSubTree(root.getRightChild()));
		}
		else
		{
			str.append("null");
		}
		str.append("]");
		return str.toString();
	}
	
	/**
	 * Returns a node descriptor.
	 * @return a node descriptor.
	 */
	public abstract String getInfo();

}
