/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.logic;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import jparfor.Functor;
import jparfor.MultiThreader;
import jparfor.Range;

import gsauln.decisiontree.DecisionTree;
import gsauln.decisiontree.Forest;
import gsauln.decisiontree.exceptions.DecisionTreeException;
import gsauln.decisiontree.interfaces.ILeafCertificationStrategy;
import gsauln.decisiontree.interfaces.ILeafCreationStrategy;
import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;
import gsauln.decisiontree.interfaces.ISplitCreationStrategy;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Gardener used to create decision trees or forests.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public class Gardener<	TP extends ITrainingPoint<P, ?>, 
P extends IPoint<?, ?>, 
LC extends Serializable, 
TS extends ITrainingState<TS, TP>> implements Serializable
{

	private static final long serialVersionUID = 1L;

	private ISplitCreationStrategy<TP, P, TS> fSplitCreator;
	private ILeafCertificationStrategy<TP, TS> fLeafCertifier;
	private ILeafCreationStrategy<TP, P, LC, TS> fLeafCreator;

	/**
	 * Creates a gardener that will use the following strategies to grow decision trees or forests.
	 * @param splitCreator used by the gardener.
	 * @param leafCertifier used by the gardener.
	 * @param leafCreator used by the gardener.
	 */
	public Gardener(ISplitCreationStrategy<TP, P, TS> splitCreator, 
			ILeafCertificationStrategy<TP, TS> leafCertifier, 
			ILeafCreationStrategy<TP, P, LC, TS> leafCreator)
	{
		fSplitCreator = splitCreator;
		fLeafCertifier = leafCertifier;
		fLeafCreator = leafCreator;
	}
	
	/**
	 * Return a decision tree created from the set of training points.
	 * @param set set of training points to create the decision tree from.
	 * @param initialTrainingState initial training state of the decision tree.
	 * @return a decision tree created from the set of training points.
	 */
	public DecisionTree<P, LC> growTree(Collection<TP> set, TS initialTrainingState)
	{
		return new DecisionTree<P, LC>(buildTree(set, initialTrainingState));
	}

	/**
	 * Return a forest of containing numberOfTrees decision trees created from the set of points and initial trainingState.
	 * @param set set of training points to create each decision tree from.
	 * @param initialTrainingState initial training state of each decision tree.
	 * @param numberOfTrees number of trees to create in the forest.
	 * @return a forest of containing numberOfTrees decision trees created from the set of points and initial trainingState.
	 */
	public Forest<P, LC> growForest(final Collection<TP> set, final TS initialTrainingState, int numberOfTrees)
	{
		Forest<P, LC> forest = new Forest<P, LC>();
		
		ArrayList<DecisionTree<P, LC>> trees = MultiThreader.foreach(new Range(numberOfTrees), new Functor<Integer, DecisionTree<P, LC>>()
				{
					@Override
					public DecisionTree<P, LC> function(Integer input)
					{
						DecisionTree<P, LC> tree = growTree(set, initialTrainingState);
						return tree;
					}
				});
		
		forest.addAll(trees);
		
		return forest;
	}
	
	/**
	 * Return the root of the decision subtree created with the set of training points and initial training state. 
	 * If the createSplitStrategy return a null split, a leaf will be created instead of an inner node.
	 * @param set set of training points to create the decision subtree from.
	 * @param trainingState initial state of training for the subtree.
	 * @return the root of the decision subtree created with the set of training points and initial training state. 
	 */
	private ANode<P, LC> buildTree(Collection<TP> set, TS trainingState)
	{
		TS newTrainingState = trainingState.update(set);

		ANode<P, LC> node = null;
		
		if (fLeafCertifier.isLeaf(set, newTrainingState))
		{
			node = fLeafCreator.createLeaf(set, newTrainingState);
		}
		else
		{
			node = createInnerNode(set, newTrainingState);
			if (node == null)
			{
				node = fLeafCreator.createLeaf(set, newTrainingState);
			}
		}
		return node;
	}

	/**
	 * Returns an inner node created with the given set of points and training state.
	 * If the createSplitStrategy return a null split, a null node will be returned, thus making build tree create
	 * a leaf instead.
	 * @param set set of training points to create the inner node from.
	 * @param trainingState state of the training of the tree at this node.
	 * @return an inner node created with the given set of points and training state.
	 */
	private ANode<P, LC> createInnerNode(Collection<TP> set, TS trainingState)
	{
		ISplit<P> split = fSplitCreator.createSplit(set, trainingState);
		if (split == null)
		{
			return null;
		}
		
		InnerNode<P, LC> node = new InnerNode<P, LC>(split);

		// Split the data set according to the best split;
		ArrayList<TP> leftSet = new ArrayList<TP>();
		ArrayList<TP> rightSet = new ArrayList<TP>();
		for (TP aT : set)
		{
			if (split.evaluate(aT.getPoint()))
			{
				rightSet.add(aT);
			}
			else
			{
				leftSet.add(aT);
			}
		}
		if (rightSet.size() == 0 || leftSet.size() == 0)
		{
			// This should never happen with the new implementation of getConstantAttributes and createSplit.
			throw new DecisionTreeException("Split created an empty set! left = "+leftSet.size()+
					", right = "+rightSet.size()+
					", "+split.toString());
		}

		// Create children
		ANode<P, LC> leftChild = buildTree(leftSet, trainingState);
		ANode<P, LC> rightChild = buildTree(rightSet, trainingState);

		// assign them to the innerNode
		node.setLeftChild(leftChild);
		node.setRightChild(rightChild);

		return node;
	}

}
