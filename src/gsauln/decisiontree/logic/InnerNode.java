/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.logic;

import java.io.Serializable;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;

/**
 * Implementation of a decision tree inner node.
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 */
public class InnerNode<P extends IPoint<?, ?>, LC extends Serializable> extends ANode<P, LC>
{

	private static final long serialVersionUID = 1L;

	private ISplit<P> fSplit;

	/**
	 * Create a new decision tree inner node that will apply the given test when queried.  If the test is true, 
	 * the right child is visited, otherwise the left child is visited.
	 * @param split split to apply when queried with a point.
	 */
	public InnerNode(ISplit<P> split)
	{
		fSplit = split;
	}
	
	@Override
	public LC evaluate(P point)
	{
		if (fSplit.evaluate(point))
		{
			return getRightChild().evaluate(point);
		}
		else
		{
			return getLeftChild().evaluate(point);
		}
	}

	@Override
	public String getInfo()
	{
		StringBuilder str = new StringBuilder();
		str.append("Inner(");
		str.append(fSplit.toString());
		str.append(")");
		return str.toString();
	}

}
