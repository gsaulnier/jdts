/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.logic;

import java.io.Serializable;

import gsauln.decisiontree.interfaces.IPoint;

/**
 * Implementation of a decision tree leaf.
 * @param <P> Type of point used with the decision tree during query.
 * @param <LC> Type of content present in the leafs of the tree.
 */
public class Leaf<P extends IPoint<?, ?>, LC extends Serializable> extends ANode<P, LC>
{

	private static final long serialVersionUID = 1L;

	private LC fLeafContent;
	
	/**
	 * Create a new DTLeaf object that will return the content of the leaf when evaluate is called.
	 * @param leafContent content of the leaf to return when evaluate is called.
	 */
	public Leaf(LC leafContent)
	{
		fLeafContent = leafContent;
	}
	
	@Override
	public LC evaluate(P point)
	{
		return fLeafContent;
	}

	@Override
	public String getInfo()
	{
		StringBuilder str = new StringBuilder();
		str.append("Leaf(");
		str.append(fLeafContent.toString());
		str.append(")");
		return str.toString();
	}

}
