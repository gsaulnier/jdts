/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split;

import java.io.Serializable;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;

/**
 * Implementation of a decision stump using points.
 * @param <P> Type of point used with the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 */
public class DecisionStump<P extends IPoint<K, Double>, K extends Serializable> implements ISplit<P>
{

	private static final long serialVersionUID = 1L;
	
	private K fAttribute;
	private double fThresold;
	
	/**
	 * Create a decision stump that will return true if the value of the attribute of the given point is above the threshold.
	 * @param attribute attribute that the decision stump will be comparing.
	 * @param threshold threshold to compare with the given attribute.
	 */
	public DecisionStump(K attribute, double threshold)
	{
		fAttribute = attribute;
		fThresold = threshold;
	}

	@Override
	public boolean evaluate(P point)
	{
		return point.getValue(fAttribute) > fThresold;
	}
	
	@Override
	public String toString()
	{
		String str = "DS("+fAttribute.toString()+", "+fThresold+")";
		return str;
	}
	
}
