/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.creation;

import java.io.Serializable;
import java.util.Collection;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.split.DecisionStump;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Interface to create decision stumps from a training set and a given attribute.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public interface IDecisionStumpCreator<	TP extends ITrainingPoint<P, ?>, 
											P extends IPoint<K, Double>, 
											K extends Serializable, 
											TS extends ITrainingState<TS, TP>> extends Serializable
{

	/**
	 * Return a decision stump created using the training set and the attribute.
	 * @param set training set to create the decision stump from.
	 * @param attribute attribute for which to create the decision stump.
	 * @param trainingState trainingState of the tree when creating this split.
	 * @return a decision stump created using the training set and the attribute.
	 */
	DecisionStump<P, K> create(Collection<TP> set, K attribute, TS trainingState);
	
}
