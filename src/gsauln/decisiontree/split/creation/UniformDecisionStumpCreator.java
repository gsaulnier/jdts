/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.creation;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.split.DecisionStump;
import gsauln.decisiontree.state.interfaces.ITrainingState;

import java.io.Serializable;
import java.util.Collection;
import java.util.Random;

import org.apache.commons.math.util.FastMath;

/**
 * Implements a decision stump creator that uses a random object to find a uniform cut point across the range of the feature.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public class UniformDecisionStumpCreator<	TP extends ITrainingPoint<P, ?>, 
								P extends IPoint<K, Double>, 
								K extends Serializable,
								TS extends ITrainingState<TS, TP>> implements IDecisionStumpCreator<TP, P, K, TS>
{

	private static final long serialVersionUID = 1L;
	
	private Random fRandom;
	
	/**
	 * Creates a new decision stump creator that uses a random object to find a uniform cut point across the range of the feature.
	 */
	public UniformDecisionStumpCreator()
	{
		fRandom = new Random();
	}
	
	/**
	 * Creates a new decision stump creator that uses the given random object to find a uniform cut point across the range of the feature.
	 * @param random random object used by this strategy.
	 */
	public UniformDecisionStumpCreator(Random random)
	{
		fRandom = random;
	}
	
	@Override
	public DecisionStump<P, K> create(Collection<TP> set, K attribute, TS trainingState)
	{
		double min = Double.POSITIVE_INFINITY;
		double max = Double.NEGATIVE_INFINITY;
		for (TP aTP : set)
		{
			double value = aTP.getPoint().getValue(attribute);
			if (value > max)
			{
				max = value;
			}
			if (value < min)
			{
				min = value;
			}
		}
		
		if (Double.isInfinite(max))
		{
			max = Double.MAX_VALUE;
		}
		if (Double.isInfinite(min))
		{
			min = -Double.MAX_VALUE;
		}
		
		double threshold = fRandom.nextDouble() * (max-min) + min;
		// Since we are creating a split, we know that there exists at least 1 number between min and max in double representation.
		// For the split to work (not create an empty split, the threshold must be < max)
		if (threshold == max)
		{
			threshold = FastMath.nextAfter(max, Double.NEGATIVE_INFINITY);
		}
		
		DecisionStump<P, K> split = new DecisionStump<P, K>(attribute, threshold);
		return split;
	}

}
