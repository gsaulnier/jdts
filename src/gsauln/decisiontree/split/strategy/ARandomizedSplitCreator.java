/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.strategy;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;
import gsauln.decisiontree.interfaces.ISplitCreationStrategy;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.split.creation.IDecisionStumpCreator;
import gsauln.decisiontree.split.strategy.scoring.interfaces.IScoringFunction;
import gsauln.decisiontree.state.interfaces.ITrainingState;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;

/**
 * Abstract class implementing all the required methods for a randomized attribute selector except for the 
 * way these attributes are selected.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public abstract class ARandomizedSplitCreator<TP extends ITrainingPoint<P, ?>, 
P extends IPoint<K, Double>, 
K extends Serializable,
TS extends ITrainingState<TS, TP>> implements ISplitCreationStrategy<TP, P, TS>
{

	private static final long serialVersionUID = 1L;

	private int fK;
	private boolean fReplacement;
	private IDecisionStumpCreator<TP, P, K, TS> fSplitCreator;
	private IScoringFunction<TP, P> fScoringFunction;
	private Random fRandom;

	/**
	 * Abstract class implementing all the required methods for a randomized attribute selector except for the 
	 * way these attributes are selected.  The split are created using the attributes selected by getRandomAttributes,
	 * then the split that maximizes the scoring function will be selected.
	 * @param k number of splits to create
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param splitCreator creator used to create the split on the randomly selected attributes.
	 * @param scoringFunction scoring function used to select the best split (maximized).
	 */
	public ARandomizedSplitCreator(int k, 
			boolean replacement, 
			IDecisionStumpCreator<TP, P, K, TS> splitCreator, 
			IScoringFunction<TP, P> scoringFunction)
	{
		init(k, replacement, splitCreator, scoringFunction, new Random());
	}

	/**
	 * Abstract class implementing all the required methods for a randomized attribute selector except for the 
	 * way these attributes are selected.  The split are created using the attributes selected by getRandomAttributes,
	 * then the split that maximizes the scoring function will be selected.
	 * @param k number of splits to create
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param splitCreator creator used to create the split on the randomly selected attributes.
	 * @param scoringFunction scoring function used to select the best split (maximized).
	 * @param random random object used to get the random attributes.
	 */
	public ARandomizedSplitCreator(int k, 
			boolean replacement, 
			IDecisionStumpCreator<TP, P, K, TS> splitCreator, 
			IScoringFunction<TP, P> scoringFunction,
			Random random)
	{
		init(k, replacement, splitCreator, scoringFunction, random);
	}

	/**
	 * Initializes a split strategy that will generate splits using the split creator by selecting k attributes
	 *  at random (with or without replacement).  Then, the split that maximizes the scoring function will be selected.
	 * @param k number of splits to create
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param splitCreator creator used to create the split on the randomly selected attributes.
	 * @param scoringFunction scoring function used to select the best split (maximized).
	 * @param random random object used to get the random attributes.
	 */
	private void init(int k, 
			boolean replacement, 
			IDecisionStumpCreator<TP, P, K, TS> splitCreator, 
			IScoringFunction<TP, P> scoringFunction,
			Random random)
	{
		fK = k;
		fReplacement = replacement;
		fSplitCreator = splitCreator;
		fScoringFunction = scoringFunction;
		fRandom = random;
	}
	
	@Override
	public ISplit<P> createSplit(Collection<TP> set, TS trainingState)
	{

		ArrayList<K> randomAttributes = getRandomAttributes(set, fK, fReplacement, trainingState, fRandom);
		ArrayList<ISplit<P>> splitList = new ArrayList<ISplit<P>>();
		for (K attribute : randomAttributes)
		{
			splitList.add(fSplitCreator.create(set, attribute, trainingState));
		}
		ISplit<P> split = SplitCreatorHelper.getBestSplit(set, splitList, fScoringFunction);
		return split;
	}

	/**
	 * Returns a list of k randomly selected attributes (with or without replacement).
	 * @param set set of training points to select random attributes from.
	 * @param k number of attributes to select.
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param trainingState training state of the tree at the node for which the function is called.
	 * @param random the given random object should be used for reproducibility of results.
	 * @return a list of k randomly selected attributes (with or without replacement).
	 */
	protected abstract ArrayList<K> getRandomAttributes(Collection<TP> set, int k, boolean replacement, TS trainingState, Random random);
	
}
