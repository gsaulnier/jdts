/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.strategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.split.creation.IDecisionStumpCreator;
import gsauln.decisiontree.split.strategy.scoring.interfaces.IScoringFunction;
import gsauln.decisiontree.state.interfaces.INonConstantAttributes;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Creates a split strategy that will generate splits using the split creator by selecting k attributes
 * at random (with or without replacement).  Then, the split that maximizes the scoring function will be selected.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 * @param <TS> Type of training state used during the creation of the trees.
 */
public class RandomizedNonConstantSplitCreator<TP extends ITrainingPoint<P, ?>, 
P extends IPoint<K, Double>, 
K extends Serializable,
TS extends INonConstantAttributes<TP, P, K> & ITrainingState<TS, TP>> 
extends ARandomizedSplitCreator<TP, P, K, TS>
{

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a split strategy that will generate splits using the split creator by selecting k non-constant attributes
	 *  at random (with or without replacement).  Then, the split that maximizes the scoring function will be selected.
	 * If no non-constant attributes are available null is returned (the gardener will then create a leaf instead).
	 * @param k number of splits to create
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param splitCreator creator used to create the split on the randomly selected attributes.
	 * @param scoringFunction scoring function used to select the best split (maximized).
	 */
	public RandomizedNonConstantSplitCreator(int k, 
			boolean replacement, 
			IDecisionStumpCreator<TP, P, K, TS> splitCreator, 
			IScoringFunction<TP, P> scoringFunction)
	{
		super(k, replacement, splitCreator, scoringFunction);
	}

	/**
	 * Creates a split strategy that will generate splits using the split creator by selecting k non-constant attributes
	 *  at random (with or without replacement).  Then, the split that maximizes the scoring function will be selected.
	 *  If no non-constant attributes are available null is returned (the gardener will then create a leaf instead).
	 * @param k number of splits to create
	 * @param replacement if true, the k attributes will be selected with replacement, if false, 
	 * the attributes will be selected without replacement.
	 * @param splitCreator creator used to create the split on the randomly selected attributes.
	 * @param scoringFunction scoring function used to select the best split (maximized).
	 * @param random random object used to get the random attributes.
	 */
	public RandomizedNonConstantSplitCreator(int k, 
			boolean replacement, 
			IDecisionStumpCreator<TP, P, K, TS> splitCreator, 
			IScoringFunction<TP, P> scoringFunction,
			Random random)
	{
		super(k, replacement, splitCreator, scoringFunction, random);
	}

	@Override
	protected ArrayList<K> getRandomAttributes(Collection<TP> set, int k, boolean replacement, TS trainingState, Random random)
	{
		ArrayList<K> attributes = trainingState.getNonConstant();
		ArrayList<K> selectedAttributes = new ArrayList<K>();

		if (!replacement)
		{
			if (k >= attributes.size())
			{
				for (K attribute : attributes)
				{
					if (!trainingState.isConstant(set, attribute))
					{
						selectedAttributes.add(attribute);
					}
				}
			}
			else
			{
				Collections.shuffle(attributes, random);
				int count = 0;
				for (K attribute : attributes)
				{
					if (count >= k)
					{
						break;
					}
					if (!trainingState.isConstant(set, attribute))
					{
						selectedAttributes.add(attribute);
						count++;
					}
				}
			}
		}
		else
		{
			boolean exit = false;
			for (int i = 0; i < k; i++)
			{
				// loop until a non constant attribute is picked
				int index = random.nextInt(attributes.size());
				while (trainingState.isConstant(set, attributes.get(index)))
				{
					attributes = trainingState.getNonConstant();
					if (attributes.size() == 0)
					{
						exit = true; // no more attributes are non constant
						break;
					}
					index = random.nextInt(attributes.size());
				}
				if (exit)
				{
					break;
				}
				selectedAttributes.add(attributes.get(index));
			}
		}

		return selectedAttributes;
	}

}
