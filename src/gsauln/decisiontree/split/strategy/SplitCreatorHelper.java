/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.strategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.split.strategy.scoring.interfaces.IScoringFunction;

/**
 * Useful functions for split creator objects.
 */
public final class SplitCreatorHelper
{

	private SplitCreatorHelper() {}
	
	/**
	 * Return the list of attributes present in the first point of the collection.
	 * @param set set of points from which to return the list of attributes.
	 * @return the list of attributes present in the first point of the collection.
	 * @param <TP> Type of training point contained in the set.
	 * @param <P> Type of point contained in the training point.
	 * @param <K> Type of keys used to identify features in the point.
	 */
	public static <TP extends ITrainingPoint<P, ?>, P extends IPoint<K, ?>, K extends Serializable> ArrayList<K> getAttributes(Collection<TP> set)
	{
		TP trainingPoint = set.iterator().next();
		HashSet<K> attributes = trainingPoint.getPoint().getAttributes();
		ArrayList<K> attributesList = new ArrayList<K>(attributes);
		return attributesList;
	}
	
	/**
	 * Return the split with maximal score according the the scoring function and set of training points.
	 * @param set training set to calculate the score of the split on.
	 * @param splits set of splits to select the optimal from.
	 * @param scoringFunction function used to assign score to splits.
	 * @return the split with maximal score according the the scoring function and set of training points.
	 * @param <TP> Type of training point contained in the set.
	 * @param <P> Type of point contained in the training point.
	 */
	public static <TP extends ITrainingPoint<P, ?>, P extends IPoint<?, ?>> ISplit<P> getBestSplit(Collection<TP> set, 
																									  ArrayList<ISplit<P>> splits, 
																									  IScoringFunction<TP, P> scoringFunction)
	{
		ISplit<P> bestSplit = null;
		double maxScore = Double.NEGATIVE_INFINITY;
		for (ISplit<P> split : splits)
		{
			double score = scoringFunction.getScore(set, split);
			if (score > maxScore || bestSplit == null)
			{
				maxScore = score;
				bestSplit = split;
			}
		}
		return bestSplit;
	}
}
