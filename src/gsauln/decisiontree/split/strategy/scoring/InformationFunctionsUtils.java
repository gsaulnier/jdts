/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.strategy.scoring;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;
import gsauln.decisiontree.interfaces.ITrainingPoint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

/**
 * Provides useful function for computing information functions.
 */
public final class InformationFunctionsUtils
{
	
	private InformationFunctionsUtils(){}
	
	/**
	 * Return the separated sets according to the split.
	 * @param set set of training point to separate.
	 * @param split split to separate the training points with.
	 * @return the separated sets according to the split.
	 * @param <TP> Type of training points used during the creation of the trees.
 	 * @param <P> Type of point used with the decision tree during query.
	 */
	public static <TP extends ITrainingPoint<P, ?>, 
					 P extends IPoint<?, ?>> HashMap<Boolean, ArrayList<TP>> performSplit(Collection<TP> set, ISplit<P> split)
	{
		HashMap<Boolean, ArrayList<TP>> separated = new HashMap<Boolean, ArrayList<TP>>();
		separated.put(true, new ArrayList<TP>());
		separated.put(false, new ArrayList<TP>());
		for (TP point : set)
		{
			Boolean splitResult = split.evaluate(point.getPoint());
			ArrayList<TP> points = separated.get(splitResult);
			points.add(point);
		}
		return separated;
	}

}
