/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.strategy.scoring;


import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.split.strategy.scoring.interfaces.IScoringFunction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.math.stat.StatUtils;


/**
 * Computes the relative variance reduction.  Useful for regression trees.
 * (Skips NaN or Inf values)
*/

/**
 * Computes the relative variance reduction.  Useful for regression trees.
 * (Skips NaN or Inf values)
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 */
public class RelativeVarianceReduction<TP extends ITrainingPoint<P, Double>, P extends IPoint<?, ?>> implements IScoringFunction<TP, P>
{

	private static final long serialVersionUID = 1L;

	@Override
	public double getScore(Collection<TP> set, ISplit<P> split)
	{
		HashMap<Boolean, ArrayList<TP>> splitSeparation = InformationFunctionsUtils.performSplit(set, split);
		
		double varS = extractVariance(set);
		double varSTrue = extractVariance(splitSeparation.get(true));
		double varSFalse = extractVariance(splitSeparation.get(false));
		
		double trueSize = splitSeparation.get(true).size();
		double falseSize = splitSeparation.get(false).size();
		double setSize = set.size();
		
		return (varS - trueSize/setSize * varSTrue - falseSize/setSize * varSFalse) / varS;
	}

	/**
	 * Return the variance of the content of the points in the set.  Skips NaN or Inf values.
	 * @param set set to compute the variance from.
	 * @return the variance of the content of the points in the set.
	 */
	protected double extractVariance(Collection<TP> set)
	{	
		ArrayList<Double> values = new ArrayList<Double>(set.size());
		for (TP trainingPoint : set)
		{
			Double value = trainingPoint.getTrainingContent();
			if (!(value.isInfinite() || value.isNaN()))
			{
				values.add(value);
			}
		}
		double[] valuesArray = new double[values.size()];
		int i = 0;
		for (double value : values)
		{
			valuesArray[i] = value;
			i++;
		}
		return StatUtils.variance(valuesArray);
	}
	
	@Override
	public String toString()
	{
		return "RVR";
	}
	
}
