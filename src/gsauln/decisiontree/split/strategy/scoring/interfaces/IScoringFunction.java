/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.split.strategy.scoring.interfaces;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ISplit;
import gsauln.decisiontree.interfaces.ITrainingPoint;

import java.io.Serializable;
import java.util.Collection;

/**
 * Interface for the calculation of a split on a training set.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 */
public interface IScoringFunction<TP extends ITrainingPoint<P, ?>, P extends IPoint<?, ?>> extends Serializable
{

	/**
	 * Returns the score of the split on the given training set.
	 * @param set set to calculate the score on.
	 * @param split split to calculate the score for.
	 * @return the score of the split on the given training set.
	 */
	double getScore(Collection<TP> set, ISplit<P> split);
	
}
