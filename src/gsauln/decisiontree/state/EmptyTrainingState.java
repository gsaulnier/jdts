/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.state;

import java.util.Collection;

import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Creates an empty training state, rendering the creation of the decision tree stateless.
 * @param <TP> Type of training points used during the creation of the trees.
 */
public class EmptyTrainingState<TP extends ITrainingPoint<?, ?>> implements ITrainingState<EmptyTrainingState<TP>, TP>
{

	@Override
	public EmptyTrainingState<TP> update(Collection<TP> set)
	{
		return this;
	}

}
