/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.state;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.apache.commons.math.util.MathUtils;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;
import gsauln.decisiontree.split.strategy.SplitCreatorHelper;
import gsauln.decisiontree.state.interfaces.INonConstantAttributes;
import gsauln.decisiontree.state.interfaces.ITrainingState;

/**
 * Provide functions to keep track of the non constant attributes in the training set while creating the tree.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 */
public class NonConstantAttributesState<TP extends ITrainingPoint<P, ?>, P extends IPoint<K, Double>, K extends Serializable> 
implements ITrainingState<NonConstantAttributesState<TP, P, K>, TP>, INonConstantAttributes<TP, P, K>
{

	private HashSet<K> fNonConstant;
	
	/**
	 * Create a new training state to keep track of non constant attributes.
	 */
	public NonConstantAttributesState()
	{
		fNonConstant = null;
	}
	
	/**
	 * Set the non constant attributes to the given list.
	 * @param attributes attributes that are non constant.
	 */
	protected void setNonConstant(HashSet<K> attributes)
	{
		fNonConstant = attributes;
	}
	
	@Override
	public ArrayList<K> getNonConstant()
	{
		return new ArrayList<K>(fNonConstant);
	}
	
	@Override
	public NonConstantAttributesState<TP, P, K> update(Collection<TP> set)
	{
		HashSet<K> newNonConstant = new HashSet<K>();
		// first time init
		if (fNonConstant == null)
		{
			fNonConstant = new HashSet<K>();
			ArrayList<K> attributes = SplitCreatorHelper.getAttributes(set);
			fNonConstant.addAll(attributes);
		}
		// clone the hash
		newNonConstant.addAll(fNonConstant);
		NonConstantAttributesState<TP, P, K> newState = new NonConstantAttributesState<TP, P, K>();
		newState.setNonConstant(newNonConstant);
		return newState;
	}
	
	@Override
	public boolean isConstant(Collection<TP> set, K attribute)
	{
		// if it is not in the set of non constant it is constant, return true
		if (!fNonConstant.contains(attribute)) 
		{
			return true;
		}
		double min = Double.POSITIVE_INFINITY;
		double max = Double.NEGATIVE_INFINITY;
		for (TP aT : set)
		{
			double value = aT.getPoint().getValue(attribute);
			if (max < value)
			{
				max = value;
			}
			if (value < min)
			{
				min = value;
			}
		}
		
		// The attribute is considered constant if there is no double between min and max (in the machine representation).
		if (MathUtils.equals(min, max, 1))
		{
			// it is constant but was still in the set of non-constant attributes, so remove it
			fNonConstant.remove(attribute);
			return true;
		}
		return false;
	}
	

}
