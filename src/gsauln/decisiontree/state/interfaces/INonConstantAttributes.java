/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.state.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import gsauln.decisiontree.interfaces.IPoint;
import gsauln.decisiontree.interfaces.ITrainingPoint;

/**
 * Interface defining functions used to keep track of the non constant attributes in the training set while creating the tree.
 * @param <TP> Type of training points used during the creation of the trees.
 * @param <P> Type of point used with the decision tree during query.
 * @param <K> Type of of keys used to access attributes. Must implement equals and hashcode.
 */
public interface INonConstantAttributes<TP extends ITrainingPoint<P, ?>, P extends IPoint<K, Double>, K extends Serializable> 
{

	/**
	 * Returns true if the attribute is constant across the training set, false otherwise.
	 * @param set set of training points for which to verify if the attribute is constant.
	 * @param attribute attribute for which to verify if it is constant.
	 * @return true if the attribute is constant across the training set, false otherwise.
	 */
	boolean isConstant(Collection<TP> set, K attribute);
	
	/**
	 * Return the list of non constant attributes present at this point in the creation of the tree.
	 * @return the list of non constant attributes present at this point in the creation of the tree.
	 */
	ArrayList<K> getNonConstant();
	
}
