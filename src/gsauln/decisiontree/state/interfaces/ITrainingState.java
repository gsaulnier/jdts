/*******************************************************************************
 * Copyright 2013 Guillaume Saulnier-Comte
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package gsauln.decisiontree.state.interfaces;

import gsauln.decisiontree.interfaces.ITrainingPoint;

import java.util.Collection;

/**
 * Interface specifying the function needed to keep a training state during the creation of the decision tree.
 * @param <TS> Type of training state implementing this interface. 
 * @param <TP> Type of training points used during the creation of the trees.
 */
public interface ITrainingState<TS extends ITrainingState<TS, TP>, TP extends ITrainingPoint<?, ?>>
{

	/**
	 * Returns an updated copy of the training state.  It is important to make a deep copy of all fields for which 
	 * the state changes. Moreover, one can use the set to update the state given by the parent.
	 * @param set set of points given by the parent to pursue the construction of the subtree.
	 * @return an updated copy of the training state.
	 */
	TS update(Collection<TP> set);
	
}
